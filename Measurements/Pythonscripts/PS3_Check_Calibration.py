#!/usr/bin/env python
import csv
import os
import sys

import numpy as numpy
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style('darkgrid')

input_voltage = [3.3, 12, 12]
sens_mVpA = [120, 120, 62.50]
#input_voltage = [20, 3.3, 12, 12]
#sens_mVpA = [120, 120, 120, 62.5]

print( f"Check calibration for:")
for index, input_v in enumerate(input_voltage):
    print(f"Sensor {index} Input voltage {input_v} V, current sensitivity {sens_mVpA[index]} mV/A")
print("\n")
if __name__ == "__main__":
    fname = sys.argv[1]

    voltages=[]
    currents=[]
    times=[]
    set_currents = []
#    data = numpy.genfromtxt(fname, dtype=None, names=True, delimiter=',')
    with open(fname, newline='') as csvfile:
        data=[]
        reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in reader:
            data.append(row)
    data = numpy.array(data[1:])
    for sensor in range(len(input_voltage)):
        volt = [float(dat) for dat in data.transpose()[5+sensor*3].flatten()]
        current = [float(dat) for dat in data.transpose()[4+sensor*3].flatten()]
        time = [float(dat) for dat in data.transpose()[1].flatten()]
        dtime = [float(dat) for dat in data.transpose()[2].flatten()]
        voltages.append((volt))
        currents.append((current))
        times.append(time)
        stri = f"Mean voltage at sensor {sensor} is {numpy.average(volt):4.4f} V,  Peak-Peak: {numpy.max(volt)-numpy.min(volt):4.4f} V" 
        stri += f" Multiply gain with : {numpy.average(volt)/input_voltage[sensor]:6.4f}"
        print(stri)
        stri = f"Mean current at sensor {sensor} is {numpy.average(current):4.4f} A, Peak-Peak: {numpy.max(current)-numpy.min(current):4.4f} A"  
        stri += f" Add to DC value {sens_mVpA[sensor]*numpy.average(current)/1000:6.4f}"
        print(stri)
   
