# PowerSensor3

PowerSensor3 is a hardware platform to monitor the power consumption (voltage and current) of different processing platforms.

The project excists of:
- SensorModule_20A, a sensor module capable of sensing up to 12V, 20A.
- BaseBoard a PCB board on which up to 4 20A sensor modules can be plugged, a WeACT STM32F401 microprocessor module and a SPI TFT 0.96" LCD Display module.

All footprints used in the project are stored in the Local_Library
More information about the boards can be found in the Docs directory of each module.

## Used Design software
For the design of both the sensor module and the baseboard KiCAD 6.0 has been used. The software can be downloaded from the KiCAD website.
https://www.kicad.org/download/

## License


